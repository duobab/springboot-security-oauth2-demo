<img src="https://gitee.com/duobab/duobab/raw/master/res/images/duo.jpg" height="100" width="200">

[![license](https://img.shields.io/github/license/seata/seata.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)

[duobab.com](http://www.duobab.com) 

## 简介

spring boot + spring security + github 基于oauth2.0授权码模式实现单点登录的最简demo
- 拉取代码
- 在github上申请一个OAuth app：https://github.com/settings/applications/new  
<img src="https://gitee.com/duobab/springboot-security-oauth2-demo/raw/master/src/main/resources/static/1.png" height="100" width="200">  

<img src="https://gitee.com/duobab/springboot-security-oauth2-demo/raw/master/src/main/resources/static/2.png" height="100" width="200">  

- 获取到client-secret、client-id后, 填入application.properties文件中
- 运行SpringbootSecurityOauth2DemoApplication类中的main方法启动
- 本地浏览器访问 http://localhost, 访问github网络比较慢, 多刷几遍
