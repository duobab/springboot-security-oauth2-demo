package com.duobab.springbootsecurityoauth2demo.controller;

import com.duobab.springbootsecurityoauth2demo.domain.UserInfo;
import com.duobab.springbootsecurityoauth2demo.security.SsoContextHolder;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller()
@RequestMapping("/")
public class IndexController {

    @RequestMapping(method = RequestMethod.GET)
    public String getIndex(Model model) throws JsonProcessingException {
        UserInfo userInfo = SsoContextHolder.getUserInfo();
        model.addAttribute("name", userInfo.getLogin());
        return "index";
    }
}
