package com.duobab.springbootsecurityoauth2demo.security;

import com.duobab.springbootsecurityoauth2demo.domain.UserInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

public class SsoContextHolder {
    public static UserInfo getUserInfo() throws JsonProcessingException {
        OAuth2Authentication authentication = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
        ObjectMapper objectMapper = new ObjectMapper();
        String authenticationJson = objectMapper.writeValueAsString(authentication.getUserAuthentication().getDetails());

        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        UserInfo userInfo = mapper.readValue(authenticationJson, new TypeReference<UserInfo>() {
        });
        return userInfo;
    }
}
