package com.duobab.springbootsecurityoauth2demo.domain;

import lombok.Data;

@Data
public class UserInfo {
    private String login;
}
